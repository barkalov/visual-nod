console.log('Clientside Javascript is started...');
import { Radio } from './uiControl/radio.js';
import { Button } from './uiControl/button.js';
import { Range } from './uiControl/range.js';
import { ValueDisplay } from './uiControl/valueDisplay.js';
import { NumberInput } from './uiControl/numberInput.js';
import { Log } from './uiControl/log.js';

import { Engine } from './engine.js';
import { Renderer } from './renderer.js';


const uiControlsHolder = document.getElementById('ui-controls');

const uiControl = {
  numberInput: {
    a: new NumberInput(uiControlsHolder, 'a', { min: 0, max: 1000 }, 32),
    b: new NumberInput(uiControlsHolder, 'b', { min: 0, max: 1000 }, 12)
  },
  button: {
    restart: new Button(uiControlsHolder, 'Запуск'),
    nextStep: new Button(uiControlsHolder, 'Следующий щаг'),
  },
  log: new Log(uiControlsHolder, 'журнал', 100),

};

const mainCanvasHolder = document.getElementById('main-canvas-holder');
const engine = new Engine(uiControl.numberInput.a.value, uiControl.numberInput.b.value);
const renderer = new Renderer(mainCanvasHolder, engine);

uiControl.button.restart.setEnabled(true);
uiControl.button.nextStep.setEnabled(false);
uiControl.numberInput.a.setEnabled(true);
uiControl.numberInput.b.setEnabled(true);

uiControl.button.restart.onClickCallback = () => {
  uiControl.button.restart.setEnabled(false);
  uiControl.button.nextStep.setEnabled(true);
  uiControl.numberInput.a.setEnabled(false);
  uiControl.numberInput.b.setEnabled(false);
  engine.reset(uiControl.numberInput.a.value, uiControl.numberInput.b.value);
  uiControl.log.clear();
  uiControl.log.add(`Начинаем всё с начала, A = ${engine.currentValue.a}, B = ${engine.currentValue.b}`);
};

uiControl.button.nextStep.onClickCallback= () => {
  engine.nextSubStep();
  if (engine.candidate.isFinished) {
    uiControl.button.restart.setEnabled(true);
    uiControl.button.nextStep.setEnabled(false);
    uiControl.numberInput.a.setEnabled(true);
    uiControl.numberInput.b.setEnabled(true);
  }
};

engine.onLogCallback = (message) => {
  uiControl.log.add(message);
};