export class Renderer {
  container = null;
  canvas = null;
  ctx = null;
  engine = null
  domSize = {
    width: null,
    height: null,
  }
  constructor(holder, engine) {
    this.holder = holder;
    this.engine = engine;
    this.engine.onSubstepDone = () => {
      this.redraw();
    }
    this.engine.onReset = () => {
      this.redraw();
    }
    this.buildDom(); // resize and redraw inside
  }
  buildDom() {
    this.container = document.createElement('div');
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.container.style.width = '100%';
    this.container.style.height = '10%';
    this.container.style.maxHeight = '200px';
    this.container.style.minHeight = '120px';
    this.canvas.style.width = '100%';
    this.canvas.style.height = '100%';

    this.container.appendChild(this.canvas);
    this.holder.appendChild(this.container);
    window.onresize = () => {
      this.resize();
    }
    this.resize();
  };
  resize() {
    this.domSize = {
      width: this.container.clientWidth,
      height: this.container.clientHeight,
    }
    this.canvas.width = this.domSize.width;
    this.canvas.height = this.domSize.height;
    this.redraw();
  }
  redraw() {
    this.clearCanvas();
    this.drawTerm('a', false);
    this.drawTerm('b', false);
    this.drawTerm('a', true);
    this.drawTerm('b', true);
  };

  clearCanvas() {
    this.ctx.clearRect(0, 0, this.domSize.width, this.domSize.height);
  }
  drawTerm(termLetter, isCurrent) {
    const textPadding = 150;

    const term = {
      origin: {
        x: textPadding,
        y: null,
      },
      baseFillColor: null,
      tailFillColor: null,
      value: null,
      div: null,
      divSize: null,
      mod: null,
    };
    if (termLetter === 'a') {
      term.origin.y = 0 + (isCurrent ? this.domSize.height / 2 : 0);
      term.baseFillColor = isCurrent? '#AA0000' : '#EEBBBB';
      term.tailFillColor = isCurrent? '#FF0000' : '#FF7777';
      term.value = isCurrent ? this.engine.currentValue.a : this.engine.initialValue.a;

    } else if (termLetter === 'b') {
      term.origin.y = this.domSize.height / 6 + (isCurrent ? this.domSize.height / 2 : 0);
      term.baseFillColor = isCurrent? '#00AA00' : '#BBEEBB';
      term.tailFillColor = isCurrent? '#00FF00' : '#77FF77';
      term.value = isCurrent ? this.engine.currentValue.b : this.engine.initialValue.b;
    } else {
      throw new Error('bad term');
    }
    let previewLesser = Math.min(this.engine.currentValue.a, this.engine.currentValue.b);
    if (isCurrent && this.engine.candidate.isValid) {
      term.div = Math.floor(term.value / this.engine.candidate.lesser);
      term.divSize = this.engine.candidate.lesser;
      term.mod = term.value % this.engine.candidate.lesser;
    } else if (!isCurrent && previewLesser) {
        term.div = Math.floor(term.value / previewLesser);
        term.divSize = previewLesser;
        term.mod = term.value % previewLesser;
    } else {
      term.div = 1;
      term.divSize = term.value;
      term.mod = 0;
    }
    let scaleFactor = (this.domSize.width - textPadding) / Math.max(this.engine.initialValue.a, this.engine.initialValue.b);

    this.ctx.save();
    this.ctx.font = '14px sans-serif';
    this.ctx.textBaseline = 'middle';
    this.ctx.fillText(`${isCurrent?'текущее ':'начальное '}${termLetter.toUpperCase()}: ${term.value}`, 0, term.origin.y + this.domSize.height / 12);
    for (let i = 0; i < term.div; i++) {
      let rect = {
        x: term.origin.x + i * term.divSize * scaleFactor,
        y: term.origin.y,
        width: term.divSize * scaleFactor,
        height: this.domSize.height / 6,
      }
      this.ctx.fillStyle = term.baseFillColor;
      this.ctx.fillRect(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2);
    }
    if (term.mod) {
      let rect = {
        x: term.origin.x + (term.value - term.mod) * scaleFactor,
        y: term.origin.y,
        width: term.mod * scaleFactor,
        height: this.domSize.height / 6,
      }
      this.ctx.fillStyle = term.tailFillColor;
      this.ctx.fillRect(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2);
    }
    this.ctx.restore();
  }
}
