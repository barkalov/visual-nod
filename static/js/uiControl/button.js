export class Button {
  name = null;
  isEnabled = null;
  holder = null;
  container = null;
  button = null;

  onClickCallback = null;
  constructor(holder, name, isEnabled = true) {
    this.name = name;
    this.holder = holder;
    this.isEnabled = isEnabled;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.button = document.createElement('button');
    this.button.innerText = this.name;
    this.updateEnabled();
    this.container.appendChild(this.button);
    this.button.onclick = () => {
      this.click();
    };
    this.holder.appendChild(this.container);
  };
  click() {
    if (this.onClickCallback) {
      this.onClickCallback();
    }
  }
  setEnabled(isEnabled) {
    this.isEnabled = isEnabled;
    this.updateEnabled();
  }
  updateEnabled() {
    this.button.disabled = !this.isEnabled;
  }
}