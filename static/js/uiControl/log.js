export class Log {
  name = null;
  holder = null;
  container = null;
  label = null;
  wall = null;
  lines = [];
  limit = 100;

  constructor(holder, name, limit) {
    this.name = name;
    this.holder = holder;
    this.limit = limit;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.label = document.createElement('div');
    this.label.classList.add('ui-control-label');
    this.label.innerText = this.name;
    this.wall = document.createElement('div');
    this.container.appendChild(this.label);
    this.container.appendChild(this.wall);
    this.holder.appendChild(this.container);
    this.updateWall();
  };
  clear() {
    this.lines.splice(0);
    this.updateWall();
  }
  add(line) {
    this.lines.push(line);
    this.lines.splice(this.limit);
    this.updateWall();
  }
  updateWall() {
    this.wall.innerText = this.lines.join('\n');;
  }
}