export class Range {
  name = null;
  limit = {
    min: null,
    max: null,
  };
  holder = null;
  container = null;
  input = null;
  gauge = null;
  value = null;
  label = null;

  onChangeCallback = null;
  constructor(holder, name, limit, value) {
    this.name = name;
    this.limit = limit;
    this.holder = holder;
    this.value = value;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.label = document.createElement('div');
    this.label.classList.add('ui-control-label');
    this.label.innerText = this.name;
    this.container.appendChild(this.label);
    this.input = document.createElement('input');
    this.gauge = document.createElement('div');
    this.input.type = 'range';
    this.input.value = this.value;
    this.input.min = this.limit.min;
    this.input.max = this.limit.max;
    this.container.appendChild(this.input);
    this.container.appendChild(this.gauge);
    this.updateGauge()
    this.input.onchange = () => {
      const value = parseFloat(this.input.value);
      this.change(value);
    };
    this.holder.appendChild(this.container);
  };
  updateGauge() {
    this.gauge.innerText = this.value.toFixed(2);
  }
  change(value) {
    this.value = value;
    this.input.value = this.value;
    this.updateGauge()
    if (this.onChangeCallback) {
      this.onChangeCallback(this.value);
    }
  }
}