export class ValueDisplay {
  name = null;
  value = null;
  holder = null;
  container = null;
  label = null;
  valueDisplay = null;

  constructor(holder, name, value) {
    this.name = name;
    this.holder = holder;
    this.value = value;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.label = document.createElement('div');
    this.label.classList.add('ui-control-label');
    this.label.innerText = this.name;
    this.labelDisplay = document.createElement('div');
    this.updateLabelDisplay();
    this.container.appendChild(this.label);
    this.container.appendChild(this.labelDisplay);
    this.holder.appendChild(this.container);
  };
  updateLabelDisplay() {
    this.labelDisplay.innerText = this.value;
  }
  change(value) {
    this.value = value;
    this.updateLabelDisplay();
  }
}