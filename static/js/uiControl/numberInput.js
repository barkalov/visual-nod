export class NumberInput {
  name = null;
  isEnabled = null;
  limit = {
    min: null,
    max: null,
  };
  holder = null;
  container = null;
  input = null;
  gauge = null;
  value = null;
  label = null;

  onChangeCallback = null;
  constructor(holder, name, limit, value, isEnabled = true) {
    this.name = name;
    this.limit = limit;
    this.holder = holder;
    this.value = value;
    this.isEnabled = isEnabled;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.label = document.createElement('div');
    this.label.classList.add('ui-control-label');
    this.label.innerText = this.name;
    this.container.appendChild(this.label);
    this.input = document.createElement('input');
    this.updateEnabled();
    this.gauge = document.createElement('div');
    this.input.type = 'number';
    this.input.value = this.value;
    this.input.min = this.limit.min;
    this.input.max = this.limit.max;
    this.container.appendChild(this.input);
    this.container.appendChild(this.gauge);
    this.updateGauge();
    this.input.onchange = () => {
      const value = parseFloat(this.input.value);
      if (!Number.isNaN(value)) {
        const limitedValue = Math.max(this.limit.min, Math.min(this.limit.max, value));
        this.change(limitedValue);
      } else {
        this.change(this.value); // fallback to prevValue (need .change to revert value in input box)
      }

    };
    this.holder.appendChild(this.container);
  };
  updateGauge() {
    this.gauge.innerText = this.value.toString();
  }
  change(value) {
    this.value = value;
    this.input.value = this.value;
    this.updateGauge();
    if (this.onChangeCallback) {
      this.onChangeCallback(this.value);
    }
  }
  setEnabled(isEnabled) {
    this.isEnabled = isEnabled;
    this.updateEnabled();
  }
  updateEnabled() {
    this.input.disabled = !this.isEnabled;
  }
}