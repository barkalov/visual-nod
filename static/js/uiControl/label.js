export class Label {
  name = null;
  holder = null;
  container = null;
  label = null;

  constructor(holder, name) {
    this.name = name;
    this.holder = holder;
    this.buildDom();
  }
  buildDom() {
    this.container = document.createElement('div');
    this.container.classList.add('ui-control-container');
    this.label = document.createElement('div');
    this.label.classList.add('ui-control-label');
    this.label.innerText = this.name;
    this.container.appendChild(this.label);
    this.holder.appendChild(this.container);
  };
}