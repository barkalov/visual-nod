
export class Engine {
  onSubstepDone = null;
  onReset = null;
  onLogCallback = null;

  initialValue = {
    a: null,
    b: null,
  }
  currentValue = {
    a: null,
    b: null,
  }
  candidate = {
    isValid: false,
    isFinal: false,
    isFinished: false,
    div: null,
    mod: null,
  }
  currentStepNum = null;
  currentSubstepNum = null;

  constructor(a, b) {
    this.reset(a, b);
  }

  reset(a, b) {
    this.initialValue.a = a;
    this.initialValue.b = b;
    this.currentValue.a = this.initialValue.a;
    this.currentValue.b = this.initialValue.b;
    this.candidate.isValid = false;
    this.candidate.isFinal = false;
    this.candidate.isFinished = false;
    this.candidate.mod = null;
    this.candidate.div = null;
    this.candidate.lesser = null;
    this.currentStepNum = 0;
    this.currentSubstepNum = 0;
    if (this.onReset) { this.onReset(); };
  }
  log(message) {
    let prettyMessage = '';
    prettyMessage += `[ШАГ:${this.currentStepNum}-${this.currentSubstepNum}] `;
    prettyMessage += message;
    if (this.onLogCallback) { this.onLogCallback(prettyMessage); } else {
      console.log(prettyMessage);
    }
  }
  async pause(msec) {
    await new Promise((resolve)=> { setTimeout(() => {
      resolve();
    }, msec);});
  }
  doSubstep0() {
    this.candidate.mod = null;
    this.candidate.div = null;
    this.candidate.lesser = null;
    this.candidate.isFinal = false;
    this.candidate.isFinished = false;
    this.candidate.isValid = false;
    this.log('Получаем на вход свежие значения A и B');
  }
  doSubstep1() {
    const resortedValue = {
      a: Math.max(this.currentValue.a, this.currentValue.b),
      b: Math.min(this.currentValue.a, this.currentValue.b),
    }
    this.currentValue = resortedValue;
    this.candidate.lesser = this.currentValue.b;
    this.log('Сортируем (меняем A и B местами, если B > A)');
  }
  doSubstep2() {
    this.candidate.mod = this.currentValue.a % this.currentValue.b;
    this.candidate.div = Math.floor(this.currentValue.a / this.currentValue.b);
    this.candidate.isValid = this.candidate.lesser !== 0;
    this.log(`Вычисляем A % B = ${this.candidate.mod}`);
  }
  doSubstep3() {
    this.candidate.isFinal = this.candidate.mod === 0;
    if (!this.candidate.isFinal) {
      this.log(`A % B != 0, по этому кладем значение остатка вместо бОльшего значения (A)`);
      this.currentValue.a = this.candidate.mod;
      this.candidate.isValid = false;
    } else {
      this.log(`A % B == 0, по этому меньшее значение ${this.currentValue.b} и есть НОД`);
    }
  }
  doSubstepReiteration() {
    this.log(`Запускаем цикл ещё раз`);
  }
  doSubstepFinal() {
    this.candidate.isFinished = true;
    this.log(`Конец`);
  }
  async nextSubStep() {
    if (this.candidate.isFinal) {
      this.doSubstepFinal();
    } else {
      let substeps = [
        this.doSubstep0,
        this.doSubstep1,
        this.doSubstep2,
        this.doSubstep3,
        this.doSubstepReiteration,
      ]
      substeps[this.currentSubstepNum].call(this);
      this.currentSubstepNum++;
      if (this.currentSubstepNum === 5) {
        this.currentStepNum++;
        this.currentSubstepNum = 0;
      }
    }
    if (this.onSubstepDone) {
      await this.pause(500);
      this.onSubstepDone();
    };
  }
}

